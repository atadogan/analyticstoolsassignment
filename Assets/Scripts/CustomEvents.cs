﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class CustomEvents : MonoBehaviour
{
   public static void FailureCount(int level, int failureCount)
    {
        AnalyticsEvent.Custom("failure_count", new Dictionary<string, object>
        {
            { "level", level },
            { "failures", failureCount}
        });
    }

    public static void MatchesInARow(int level, int maxMatchesInARow)
    {
        AnalyticsEvent.Custom("matches_in_a_Row", new Dictionary<string, object>
        {
            { "level", level },
            { "matchesinarow", maxMatchesInARow}
        });
    }

    public static void TimeToCompleteLevelEvent(int level, float time)
    {
        AnalyticsEvent.Custom("time_to_complete", new Dictionary<string, object>
        {
            { "level", level },
            { "time", time}
        });
    }

    public static void TimeToFirstMatch(int level, float time)
    {
        AnalyticsEvent.Custom("time_to_first_match", new Dictionary<string, object>
        {
            { "level", level },
            { "time", time}
        });
    }

    public static void TotalFlipsToCompletion (int level, int flips)
    {
        AnalyticsEvent.Custom("total_flip_count", new Dictionary<string, object>
        {
            { "level", level },
            { "flips", flips}
        });
    }

}
