﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    private GameManager m_manager;
    public GameManager.Symbols Symbol { get; private set; }
    private Transform m_child;
    private Collider m_collider;

    //Assigning collider and getting child
    private void Start()
    {
        m_child = this.transform.GetChild( 0 );
        m_collider = GetComponent<Collider>();
    }

    //Tell manager that this tile was selected
    private void OnMouseDown()
    {
        m_manager.TileSelected( this );
    }

    //initializes tile and connects manager and symbol to it
    public void Initialize( GameManager manager, GameManager.Symbols symbol )
    {
        m_manager = manager;
        Symbol = symbol;
    }

    //Show Tile 
    public void Reveal()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 180, 0.8f ) );
        m_collider.enabled = false;
    }

    //Hide Tile
    public void Hide()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 0, 0.8f ) );
        m_collider.enabled = true;
    }

    //Visualize the spin of the tile
    private IEnumerator Spin( float target, float time )
    {
        float timer = 0;
        float startingRotation = m_child.eulerAngles.y;
        Vector3 euler = m_child.eulerAngles;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            euler.y = Mathf.LerpAngle( startingRotation, target, timer / time );
            m_child.eulerAngles = euler;
            yield return null;
        }
        euler.y = target;
        m_child.eulerAngles = euler;
    }
}
