﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;

    [System.Flags]
    public enum Symbols
    {
        Waves =             1 << 0,
        Dot =               1 << 1,
        Square =            1 << 2,
        LargeDiamond =      1 << 3,
        SmallDiamonds =     1 << 4,
        Command =           1 << 5,
        Bomb =              1 << 6,
        Sun =               1 << 7,
        Bones =             1 << 8,
        Drop =              1 << 9,
        Face =              1 << 10,
        Hand =              1 << 11,
        Flag =              1 << 12,
        Disk =              1 << 13,
        Candle =            1 << 14,
        Wheel =             1 << 15
    }

    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;

    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;



    public bool autoPlay = false;
    private void Update()
    {
        if (autoPlay)
        {
            SetTimeScale(5);
            AutoPlay();
        }
        else
        {
            SetTimeScale(1);
        }
    }

    int randomTile;
    GameObject tile;
    List<GameObject> allTiles = new List<GameObject>();
    //Autoplay bot code
    void AutoPlay()
    {
        randomTile = UnityEngine.Random.Range(0, allTiles.Count);
        tile = allTiles[randomTile];
        if (tile.GetComponent<BoxCollider>().enabled)
        {
            TileSelected(tile.GetComponent<Tile>());
        }
    }

    //sets the timescale only when it is changed
    int currentTimeScale = 0;
    void SetTimeScale(float timeScaleValue)
    {
        if(timeScaleValue != currentTimeScale)
        {
            Time.timeScale = timeScaleValue;
        }
    }

    private void Start()
    {
        LoadLevel( CurrentLevelNumber );
    }

    //Load a new level
    private void LoadLevel( int levelNumber )
    {
        LevelDescription level = Levels[levelNumber % Levels.Length];

        List<Symbols> symbols = GetRequiredSymbols( level );

        //instantiate all the tiles in correct position
        for ( int rowIndex = 0; rowIndex < level.Rows; ++rowIndex )
        {
            float yPosition = rowIndex * ( 1 + TileSpacing );
            for ( int colIndex = 0; colIndex < level.Columns; ++colIndex )
            {
                float xPosition = colIndex * ( 1 + TileSpacing );
                GameObject tileObject = Instantiate( TilePrefab, new Vector3( xPosition, yPosition, 0 ), Quaternion.identity, this.transform );
                int symbolIndex = UnityEngine.Random.Range( 0, symbols.Count );
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol( symbols[symbolIndex] );
                tileObject.GetComponent<Tile>().Initialize( this, symbols[symbolIndex] );
                symbols.RemoveAt( symbolIndex );
                //add tiles to autoplay
                allTiles.Add(tileObject);
            }
        }

        SetupCamera( level );

        startTime = Time.time;
    }

    //Return a list of Sysmbols in the Level
    private List<Symbols> GetRequiredSymbols( LevelDescription level )
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //make sure there are an even number of symbols
        {
            Array allSymbols = Enum.GetValues( typeof( Symbols ) );

            m_cardsRemaining = cardTotal;

            if ( cardTotal % 2 > 0 )
            {
                new ArgumentException( "There must be an even number of cards" );
            }

            foreach ( Symbols symbol in allSymbols )
            {
                if ( ( level.UsedSymbols & symbol ) > 0 )
                {
                    symbols.Add( symbol );
                }
            }
        }

        //check if any symbols set or if there are too many
        {
            if ( symbols.Count == 0 )
            {
                new ArgumentException( "The level has no symbols set" );
            }
            if ( symbols.Count > cardTotal / 2 )
            {
                new ArgumentException( "There are too many symbols for the number of cards." );
            }
        }

        //add all required symbols 
        {
            int repeatCount = ( cardTotal / 2 ) - symbols.Count;
            if ( repeatCount > 0 )
            {
                List<Symbols> symbolsCopy = new List<Symbols>( symbols );
                List<Symbols> duplicateSymbols = new List<Symbols>();
                for ( int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex )
                {
                    int randomIndex = UnityEngine.Random.Range( 0, symbolsCopy.Count );
                    duplicateSymbols.Add( symbolsCopy[randomIndex] );
                    symbolsCopy.RemoveAt( randomIndex );
                    if ( symbolsCopy.Count == 0 )
                    {
                        symbolsCopy.AddRange( symbols );
                    }
                }
                symbols.AddRange( duplicateSymbols );
            }
        }

        symbols.AddRange( symbols );

        return symbols;
    }

    //Returns the offset of the symbol in Vector2
    private Vector2 GetOffsetFromSymbol( Symbols symbol )
    {
        Array symbols = Enum.GetValues( typeof( Symbols ) );
        for ( int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex )
        {
            if ( ( Symbols )symbols.GetValue( symbolIndex ) == symbol )
            {
                return new Vector2( symbolIndex % 4, symbolIndex / 4 ) / 4f;
            }
        }
        Debug.Log( "Failed to find symbol" );
        return Vector2.zero;
    }

    //Setup camera to include all tiles
    private void SetupCamera( LevelDescription level )
    {
        Camera.main.orthographicSize = ( level.Rows + ( level.Rows + 1 ) * TileSpacing ) / 2;
        Camera.main.transform.position = new Vector3( ( level.Columns * ( 1 + TileSpacing ) ) / 2, ( level.Rows * ( 1 + TileSpacing ) ) / 2, -10 );
    }

    //store the selected tile and wait for second tile to be selected
    public void TileSelected( Tile tile )
    {
        if ( m_tileOne == null )
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        else if ( m_tileTwo == null )
        {
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            if ( m_tileOne.Symbol == m_tileTwo.Symbol )
            {
                StartCoroutine( WaitForHide( true, 1f ) );
            }
            else
            {
                StartCoroutine( WaitForHide( false, 1f ) );
            }
        }

        flipCount++;
    }

    //Load the next Level
    private void LevelComplete()
    {
        ++CurrentLevelNumber;
        if ( CurrentLevelNumber > Levels.Length - 1 )
        {
            Debug.Log( "GameOver" );
        }
        else
        {
            SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
        }
    }

    //Checks if cards are a match and waits for cards to finish animating into hidden state
    //if no cards are left completes the level
    private IEnumerator WaitForHide( bool match, float time )
    {
        float timer = 0;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            if ( timer >= time )
            {
                break;
            }
            yield return null;
        }
        if ( match )
        {
            allTiles.Remove(m_tileOne.gameObject);
            allTiles.Remove(m_tileTwo.gameObject);

            Destroy( m_tileOne.gameObject );
            Destroy( m_tileTwo.gameObject );
            m_cardsRemaining -= 2;
            if (lastOneMatched)
            {
                matchesInARow++;
            }

            lastOneMatched = true;
            if (!matched)
            {
                firstMatchTime = Time.time;
                matched = true;
            }
        }
        else
        {
            if (lastOneMatched)
            {
                matchesInARow = 0;
                lastOneMatched = false;
            }
            failureCount++;
            m_tileOne.Hide();
            m_tileTwo.Hide();
        }
        m_tileOne = null;
        m_tileTwo = null;

        if ( m_cardsRemaining == 0 )
        {
            endTime = Time.time;
            SendData();
            LevelComplete();
        }
    }

    bool lastOneMatched = false;
    bool matched = false;
    float startTime, endTime;
    float firstMatchTime;


    int failureCount = 0;
    int matchesInARow = 0;
    int flipCount = 0;

    void SendData()
    {
        CustomEvents.MatchesInARow(CurrentLevelNumber, matchesInARow + 1);
        CustomEvents.FailureCount(CurrentLevelNumber, failureCount);
        CustomEvents.TimeToCompleteLevelEvent(CurrentLevelNumber, endTime - startTime);
        CustomEvents.TimeToFirstMatch(CurrentLevelNumber, firstMatchTime - startTime);
        CustomEvents.TotalFlipsToCompletion(CurrentLevelNumber, flipCount);

        //reset Data
        failureCount = 0;
        matchesInARow = 0;
        flipCount = 0;
    }

}
